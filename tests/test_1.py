import pytest
import allure
import time


class Test1:
    @allure.feature('фича1')
    @allure.story('тест1')
    def test_1(first_fixture):
        time.sleep(3)

    @allure.feature('фича1')
    @allure.story('тест2')
    def test_2(first_fixture):
        time.sleep(1)

    @allure.feature('фича1')
    @allure.story('тест3')
    def test_3(first_fixture):
        time.sleep(1)


class Test2:
    @allure.feature('фича2')
    @allure.story('тест4')
    def test_4(first_fixture):
        time.sleep(1)

    @allure.feature('фича2')
    @allure.story('тест5')
    def test_5(first_fixture):
        pass
