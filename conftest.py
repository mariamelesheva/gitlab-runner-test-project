import pytest
import requests
import allure


# class ApiClient:
#     def __init__(self, base_address):
#         self.base_address = base_address
#
#     def post(self, path="/", params=None, data=None, json=None, headers=None):
#         url = f"{self.base_address}{path}"
#         with allure.step(f'POST request to: {url}'):
#             return requests.post(url=url, params=params, data=data, json=json, headers=headers)
#
#     def get(self, path="/", params=None, headers=None):
#         url = f"{self.base_address}{path}"
#         with allure.step(f'GET request to: {url}'):
#             return requests.get(url=url, params=params, headers=headers)


@pytest.fixture
def first_fixture(get_test_name):
    with allure.step(f'Setup from {get_test_name}'):
        print(f'Setup from {get_test_name}')
    yield
    with allure.step(f'Teardown from {get_test_name}'):
        print(f'Teardown from {get_test_name}')


@pytest.fixture(scope='function')
def get_test_name(request):
    return request.node.name
